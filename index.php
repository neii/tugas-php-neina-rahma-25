
 <!DOCTYPE html>
<html lang="en">
<head>
	<title>BLOG | Login</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <style type="text/css">
    	#pesan {
    		font-size: 15px;
    	}
    </style>
</head>
<body class="body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-6 col-md-3">
			    <div class="card" style="width: 25rem;">
    				<div class="card-body">
					    
						<nav class="navbar navbar-expand-lg navbar-dark bg-success">
							<h6 class="display-6"><strong>Login</strong></h6>
						  	<div class="collapse navbar-collapse" id="navbarNavDropdown">

						  	</div>
						</nav>
						<?php 
						if (isset($_GET["pesan"])) {
							$pesan = $_GET["pesan"];
							?>
							<div class="alert alert-success" role="alert">
						    <span id="pesan"><?=$pesan; ?></span>
							</div>
						<?php 
							}
						 ?>

					    <form method="POST" action="proses_login.php">
					    	<div class="mb-3">
							 	 <label  class="form-label" id="formGroupExampleInput" placeholder="username">Username</label>
							  	 <input type="text" class="form-control" name="username" required>
							</div>
						 	<div class="mb-3">
					 	 		<label class="form-label" id="formGroupExampleInput" placeholder="password">Password</label>
						 		<input type="password" class="form-control" name="password" required>
							</div>
							<div class="btn btn-success">
					        	<button class="btn btn-success" type="submit">Login</button><br>
					        </div>
						</form>
								 Belum punya Akun? <a href="register.php">Daftar Akun</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
