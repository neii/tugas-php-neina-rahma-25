<?php 

include 'koneksi.php';

$id = $_GET["id"];

$sql = "SELECT * FROM users WHERE id='$id'";
$result = $koneksi->query($sql);
$hasil = $result->fetch_assoc();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Ubah data</title>
 	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <style type="text/css">
    	#pesan {
    		color: red;
    		font-size: 15px;
    	}
    </style>
 </head>
 <body class="body">
 	<div class="container1">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-6 col-md-3">
    			<div class="card" style="width: 20rem;">
    				<div class="card-body">
						<nav class="navbar navbar-expand-lg navbar-dark bg-success">
							<h6 class="display-6">Data</h6>
					  		<div class="collapse navbar-collapse" id="navbarNavDropdown">
							</div>
						</nav>
						<?php 
						if (isset($_GET["pesan"])) {
							$pesan = $_GET["pesan"]; 
							?>
							<div class="alert alert-success" role="alert">
							    <span id="pesan"><?=$pesan; ?></span>
							</div>
						<?php
							}
						 ?>
						<form method="POST" action="proses_ubah.php">
							<input type="hidden" name="id" value="<?= $id?>">
							<table class="table table-table dark">
							 <div class="mb-3">
							 	 <label  class="form-label" id="formGroupExampleInput" placeholder="nama">Nama</label>
							  	 <input value="<?= $hasil["nama"]?>" type="text" class="form-control" name="nama" required>
							</div>
							 <div class="mb-3">
							 	 <label  class="form-label" id="formGroupExampleInput" placeholder="username">Username</label>
							  	 <input value="<?= $hasil["username"]?>" type="text" class="form-control" name="username" required>
						 	</div>
						 	<div class="mb-3">
							 	 <label  class="form-label" id="formGroupExampleInput" placeholder="email">Email</label>
							  	 <input value="<?= $hasil["email"]?>" type="text" class="form-control" name="email" required>
						 	</div>
						 </table>
								 <button class="btn btn-success" type="submit">Kirim</button><br>
					 	</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	


	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
 </body>
 </html>
